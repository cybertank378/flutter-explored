import 'package:freezed_annotation/freezed_annotation.dart';

part 'onboard.freezed.dart';

@freezed
class OnBoard with _$OnBoard {
  const OnBoard._();

  factory OnBoard(
      {required String image,
      required String title,
      required String description}) = _OnBoard;
}
