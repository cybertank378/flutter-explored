// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'onboard.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$OnBoard {
  String get image => throw _privateConstructorUsedError;
  String get title => throw _privateConstructorUsedError;
  String get description => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $OnBoardCopyWith<OnBoard> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OnBoardCopyWith<$Res> {
  factory $OnBoardCopyWith(OnBoard value, $Res Function(OnBoard) then) =
      _$OnBoardCopyWithImpl<$Res, OnBoard>;
  @useResult
  $Res call({String image, String title, String description});
}

/// @nodoc
class _$OnBoardCopyWithImpl<$Res, $Val extends OnBoard>
    implements $OnBoardCopyWith<$Res> {
  _$OnBoardCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? image = null,
    Object? title = null,
    Object? description = null,
  }) {
    return _then(_value.copyWith(
      image: null == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String,
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      description: null == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_OnBoardCopyWith<$Res> implements $OnBoardCopyWith<$Res> {
  factory _$$_OnBoardCopyWith(
          _$_OnBoard value, $Res Function(_$_OnBoard) then) =
      __$$_OnBoardCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String image, String title, String description});
}

/// @nodoc
class __$$_OnBoardCopyWithImpl<$Res>
    extends _$OnBoardCopyWithImpl<$Res, _$_OnBoard>
    implements _$$_OnBoardCopyWith<$Res> {
  __$$_OnBoardCopyWithImpl(_$_OnBoard _value, $Res Function(_$_OnBoard) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? image = null,
    Object? title = null,
    Object? description = null,
  }) {
    return _then(_$_OnBoard(
      image: null == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String,
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      description: null == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_OnBoard extends _OnBoard {
  _$_OnBoard(
      {required this.image, required this.title, required this.description})
      : super._();

  @override
  final String image;
  @override
  final String title;
  @override
  final String description;

  @override
  String toString() {
    return 'OnBoard(image: $image, title: $title, description: $description)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_OnBoard &&
            (identical(other.image, image) || other.image == image) &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.description, description) ||
                other.description == description));
  }

  @override
  int get hashCode => Object.hash(runtimeType, image, title, description);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_OnBoardCopyWith<_$_OnBoard> get copyWith =>
      __$$_OnBoardCopyWithImpl<_$_OnBoard>(this, _$identity);
}

abstract class _OnBoard extends OnBoard {
  factory _OnBoard(
      {required final String image,
      required final String title,
      required final String description}) = _$_OnBoard;
  _OnBoard._() : super._();

  @override
  String get image;
  @override
  String get title;
  @override
  String get description;
  @override
  @JsonKey(ignore: true)
  _$$_OnBoardCopyWith<_$_OnBoard> get copyWith =>
      throw _privateConstructorUsedError;
}
