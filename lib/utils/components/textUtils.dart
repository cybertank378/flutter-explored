
import 'package:flutter/material.dart';

class TextComponent extends StatelessWidget {


  final String text;
  final Color textColor;
  final double fontSize;
  final FontWeight fontWeight;
  final TextAlign textAlign;
  final int headingLevel;

  const TextComponent({super.key,
    required this.text,
    this.textColor = Colors.black, // Default text color is black
    this.fontSize = 12, // Default font size is 12
    this.fontWeight = FontWeight.normal, // Default font weight is normal
    this.textAlign = TextAlign.start, // Default text alignment is start (left-aligned)
    this.headingLevel = 0, // Default heading level is 0 (regular text)
  });


  @override
  Widget build(BuildContext context) {
    // Define text styles for each heading level
    TextStyle? textStyle;
    switch (headingLevel) {
      case 1:
        textStyle = TextStyle(
          color: textColor,
          fontSize: 32,
          fontWeight: FontWeight.w700,
        );
        break;
      case 2:
        textStyle = TextStyle(
          color: textColor,
          fontSize: 28,
          fontWeight: FontWeight.w600,
        );
        break;
      case 3:
        textStyle = TextStyle(
          color: textColor,
          fontSize: 24,
          fontWeight: FontWeight.w500,
        );
        break;
      case 4:
        textStyle = TextStyle(
          color: textColor,
          fontSize: 20,
          fontWeight: FontWeight.w400,
        );
        break;
      case 5:
        textStyle = TextStyle(
          color: textColor,
          fontSize: 18,
          fontWeight: FontWeight.w300,
        );
        break;
      case 6:
        textStyle = TextStyle(
          color: textColor,
          fontSize: 16,
          fontWeight: FontWeight.w200,
        );
        break;
      default:
        textStyle = TextStyle(
          color: textColor,
          fontSize: fontSize,
          fontWeight: fontWeight,
        );
    }

    return Text(
      text,
      style: textStyle,
      textAlign: textAlign,
    );
  }
}
