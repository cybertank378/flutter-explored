import 'package:flutter/material.dart';

import '../../models/onboard.dart';
import '../../utils/components/textUtils.dart';

class OnBoardContent extends StatelessWidget {
  final OnBoard onBoardItems;
  const OnBoardContent({super.key, required this.onBoardItems});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: MediaQuery.of(context).size.height / 1.80,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(onBoardItems.image), fit: BoxFit.fill)),
        ),
        TextComponent(
            text: onBoardItems.title,
            headingLevel: 1,
            textAlign: TextAlign.center),
        const SizedBox(
          height: 16,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 40, right: 40),
          //apply padding horizontal or vertical only
          child: TextComponent(
              text: onBoardItems.description,
              fontSize: 14,
              textAlign: TextAlign.center),
        ),
      ],
    );
  }
}
