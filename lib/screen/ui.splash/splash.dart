import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:untitled/screen/ui.entrance/Entrance.dart';

import '../ui.home/Home.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersive);
    Future.delayed(const Duration(seconds: 2), () {
      Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (_) => const EntranceScreen()
      ));
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFFFFFFF),
      body: Container(
        width: double.infinity,
        decoration: const BoxDecoration(color: Color(0xFFFFFFFF)),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: 268,
                height: 195,
                child: Image.asset('./assets/images/bcanow.png'),
              ),
            ]),
      ),
    );
  }
}
