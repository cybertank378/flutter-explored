import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../models/onboard.dart';
import '../ui.login/login.dart';
import '../widgets/onBoard.dart';

final List<OnBoard> demoData = [
  OnBoard(
      image: "./assets/images/onboard/onboard1.png",
      title: "Title 01",
      description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."),
  OnBoard(
    image: "./assets/images/onboard/onboard2.png",
    title: "Title 02",
    description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  ),
  OnBoard(
    image: "./assets/images/onboard/onboard3.png",
    title: "Title 03",
    description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  ),
  OnBoard(
      image: "./assets/images/onboard/onboard4.png",
      title: "Title 04",
      description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.")
];

class EntranceScreen extends StatefulWidget {
  const EntranceScreen({super.key});

  @override
  State<EntranceScreen> createState() => _EntranceScreenState();
}

class _EntranceScreenState extends State<EntranceScreen> {
  final int _numPages = 4;
  final PageController _pageController = PageController(initialPage: 0);

  int _currentPage = 0;

  List<Widget> _buildPageIndicator() {
    List<Widget> list = [];
    for (int i = 0; i < _numPages; i++) {
      list.add(i == _currentPage ? _indicator(true) : _indicator(false));
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.light,
          child: Flex(
            direction: Axis.horizontal,
            children: [
              Expanded(
                child: Column(
                  children: [
                    Expanded(
                      flex: 4,
                      child: PageView.builder(
                        onPageChanged: (int page) {
                          setState(() {
                            _currentPage = page;
                          });
                        },
                        itemCount: demoData.length,
                        controller: _pageController,
                        itemBuilder: (context, index) =>
                            OnBoardContent(onBoardItems: demoData[index]),
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: _buildPageIndicator()),
                    const SizedBox(
                      height: 48,
                    ),
                    getButtonStyle(_currentPage),
                    const SizedBox(
                      height: 79,
                    ),
                  ],
                ),
              )
            ],
          )),
    );
  }

  Widget _indicator(bool isActive) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 150),
      margin: const EdgeInsets.symmetric(horizontal: 8.0),
      height: 8.0,
      width: isActive ? 8.0 : 8.0,
      decoration: BoxDecoration(
        color: isActive ? const Color(0xFFF3A626) : Colors.grey,
        border: isActive ? null : Border.all(color: const Color(0xFFF3A626)),
        borderRadius: const BorderRadius.all(
          Radius.circular(20),
        ),
      ),
    );
  }

  getButtonStyle(int currentPage) {
    if (currentPage <= 2) {
      print("Login To Page $currentPage");
      return ElevatedButton(
        style: ElevatedButton.styleFrom(
          fixedSize: const Size(300, 48),
          side: const BorderSide(color: Color(0xFFF3A626), width: 2),
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
        ),
        onPressed: () {
          _pageController.nextPage(
            duration: const Duration(milliseconds: 500),
            curve: Curves.easeInSine,
          );
        },
        child: const Padding(
          padding: EdgeInsets.all(8),
          child: Text('Lanjutkan',
              style: TextStyle(
                  fontFamily: 'RedHatDisplay',
                  color: Color(0xFFF3A626),
                  fontSize: 18,
                  fontWeight: FontWeight.w500)),
        ),
      );
    } else {
      return ElevatedButton(
          style: ElevatedButton.styleFrom(
            fixedSize: const Size(300, 48),
            backgroundColor: const Color(0xFFF3A626),
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(25))),
          ),
          onPressed: () => {
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (_) => const LoginScreen()))
              },
          child: const Text("Mulai Sekarang",
              style: TextStyle(
                  fontSize: 18,
                  fontFamily: 'RedHatDisplay',
                  fontWeight: FontWeight.w700,
                  color: Color(0xFFFFFFFF))));
    }
  }
}
