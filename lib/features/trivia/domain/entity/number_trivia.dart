import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class NumberTrivia extends Equatable {
  String? text;
  int? number;

  NumberTrivia({
    @required this.text,
    @required this.number,
  });

  @override
  List<Object?> get props =>  [
    text,
    number,
  ];

}